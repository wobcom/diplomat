# Changelog

# v0.1.1 (2020-03-03)

- Move to explicit line-buffering and text mode everywhere
- Add `cmd` and `env` as instance properties to diplomat

# v0.1.0 (2020-03-03)

- Initial version
