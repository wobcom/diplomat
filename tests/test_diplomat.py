import io
import shutil
import subprocess
import tempfile
import time
import types

import pytest

from diplomat import Attachee, Diplomat


def test_diplomat():
    ran = False

    def exit_fn(job):
        nonlocal ran
        ran = True

    def pre_start(job):
        assert not job.is_running()

    def post_start(job):
        assert job.is_running()

    diplo = Diplomat(
        "echo",
        "Hello World",
        on_exit=exit_fn,
        pre_start=pre_start,
        post_start=post_start,
    )

    assert diplo.cmd == ("echo", "Hello World")
    assert diplo.is_running()
    assert isinstance(diplo.process(), subprocess.Popen)
    assert diplo.poll() == diplo.process().returncode
    assert isinstance(diplo.output_file(), tempfile._TemporaryFileWrapper)
    assert isinstance(diplo.error_file(), tempfile._TemporaryFileWrapper)

    tmpdir = tempfile.gettempdir()
    assert diplo.output_file_name().startswith(tmpdir)
    assert diplo.error_file_name().startswith(tmpdir)

    diplo.wait()

    assert not diplo.is_running()

    assert diplo.error() == ""
    assert diplo.output() == "Hello World\n"

    assert diplo.has_succeeded()

    diplo.terminate()
    assert diplo.has_succeeded()

    assert ran, "Diplomat.on_exit runs on exit"


def test_reusing_diplomat():
    diplo1 = Diplomat("echo", "Hello World")
    diplo1.wait()

    diplo2 = Diplomat(
        "echo",
        "And also you",
        out=diplo1.output_file_name(),
        err=diplo1.error_file_name(),
    )
    assert diplo2.is_running()

    diplo2.wait()

    assert diplo2.error() == ""
    assert diplo2.output() == "Hello World\nAnd also you\n"

    assert diplo2.has_succeeded()


@pytest.mark.skipif(shutil.which("read") == None, reason="test requires `read` executable")
def test_diplomat_input():
    diplo = Diplomat(shutil.which("read"))
    assert diplo.is_running()

    diplo.write(b"hi\n")
    diplo.wait()
    assert diplo.has_succeeded()


def test_diplomat_single_file():
    diplo = Diplomat("echo", "hi", single_file=True)
    assert diplo.error_file_name() == diplo.output_file_name()
    assert diplo.error() == diplo.output()


def test_diplomat_env():
    diplo = Diplomat(
        "python", "-c", "import os; print(os.getenv('X'))", env={"X": "hi"}
    )
    diplo.wait()
    assert "hi\n" == diplo.output()


def test_failing_diplomat():
    diplo = Diplomat("this-command-does-not-exist")

    assert not diplo.is_running()

    assert diplo.has_failed()

    with pytest.raises(FileNotFoundError):
        diplo.poll()

    with pytest.raises(FileNotFoundError):
        diplo.process()

    with pytest.raises(FileNotFoundError):
        diplo.wait()

    with pytest.raises(FileNotFoundError):
        diplo.terminate()


def test_attachee():
    # let’s create an attachee from a diplomat, to get real files
    diplo = Diplomat("echo", "Hello Attachee\nMultiline", single_file=True)
    diplo.wait()
    att = diplo.to_attachee()

    assert isinstance(att.output_file(), io.TextIOWrapper)
    assert isinstance(att.error_file(), io.TextIOWrapper)

    tmpdir = tempfile.gettempdir()
    assert att.output_file_name().startswith(tmpdir)
    assert att.error_file_name().startswith(tmpdir)

    assert att.error() == "Hello Attachee\nMultiline\n"
    assert att.output() == "Hello Attachee\nMultiline\n"

    gen = att.output_stream()
    assert isinstance(gen, types.GeneratorType)
    assert ["Hello Attachee\n", "Multiline\n"] == list(gen)

    gen = att.error_stream()
    assert isinstance(gen, types.GeneratorType)
    assert ["Hello Attachee\n", "Multiline\n"] == list(gen)
